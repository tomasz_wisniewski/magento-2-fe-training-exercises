var screenWidth = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;

require([
    'jquery',
    'collapsible'
], function ($) {
    'use strict';

    $(document).ready(function(){
        if (screenWidth <= 768) {
            $('.accordion').collapsible({
                header: ".block-title",
                content: ".block-content",
                collapsible: true,
                animate: 300,
                icons: {"header": "plus", "activeHeader": "minus"}
            });
        }
    });
});